# README #

Amdatu Jedis makes using the [Jedis](https://github.com/xetorthio/jedis) library, which is a "small and sane Redis client", easier in OSGi. Amdatu Jedis provides a Managed Service (org.amdatu.jedis) that can be configured with a Redis host and port. It than provides a JedisService, which gives access to the Jedis API, but also adds convenience methods to work with Redis as a cache. Also, it supports an RX based API to work with Redis channels. 

A common usage example is the following:


```
#!java
//Uses connection pooling and cleans up resources automatically.
m_jedisService.runWithJedis(jedis -> jedis.del(SOME_KEY));
```

To work with Redis as a cache:

```
#!java
//Either returns the list of MyType items from cache, or if not cached, invoke the producer method to fetch the data elsewhere and store it in cache.
return m_jedisService.listFromCache(KEYS_OF_CACHE, MyType.class, () -> return mydb.list());
```

To work with channels:

```
#!java

subscriber = m_jedisService.observe(jedisTopic).subscribe(value -> {
   // do useful stuff when a value is posted to this topic. E.g. invalidate your local caches.
   // useful as a simple pub/sub mechanism
});
```