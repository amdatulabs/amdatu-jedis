package org.amdatu.jedis.test;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.amdatu.jedis.JedisService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import rx.observers.TestSubscriber;

public class JedisServiceTest {

	private volatile JedisService serviceToTest;
	private TestSubscriber<String> testSubscriber = new TestSubscriber<>();

	@Before
	public void setup() {
		configure(this)
		.add(createConfiguration("org.amdatu.jedis").set("host", "192.168.99.100").set("port", "6379"))
		.add(createServiceDependency().setService(JedisService.class).setRequired(true)).apply();
	}

	@Test
	public void testObserve() throws InterruptedException {
		serviceToTest.observe("testchannel").subscribe(testSubscriber);
		TimeUnit.SECONDS.sleep(1);
		serviceToTest.runWithJedis(redis -> redis.publish("testchannel", "testvalue"));
		TimeUnit.SECONDS.sleep(1);
		testSubscriber.assertReceivedOnNext(Arrays.asList("testvalue"));
	}

	@After
	public void after() {

		testSubscriber.unsubscribe();
		cleanUp(this);

	}
}