/*
 * Copyright (c) 2016 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jedis.factory;

import java.io.IOException;
import java.util.Dictionary;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.function.Supplier;

import org.amdatu.jedis.JedisService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ConfigurationDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.apache.felix.dm.annotation.api.Stop;
import org.osgi.service.cm.ConfigurationException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;
import rx.Observable;
import rx.subscriptions.Subscriptions;

@Component
public class JedisConnectionPool implements JedisService {

	private final Executor m_subscribePool = Executors.newCachedThreadPool();
	private volatile JedisPool m_pool;
	private volatile String m_host;
	private volatile int m_port;

	@Start
	public void start() {
		m_pool = new JedisPool(new JedisPoolConfig(), m_host, m_port);
	}

	@Stop
	public void stop() {
		m_pool.destroy();
	}

	@Override
	public Jedis getJedis() {
		return m_pool.getResource();
	}

	@Override
	public <R> R runWithJedis(Function<Jedis, R> func) {
		try (Jedis jedis = m_pool.getResource()) {
			return func.apply(jedis);
		} catch(Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	@Override
	public <T> List<T> listFromCache(String key, Class<T> type, Supplier<List<T>> retrieveFunc) {
		String cached = runWithJedis(jedis -> jedis.get(key));
		ObjectMapper mapper = new ObjectMapper();
		List<T> result = null;

		if (cached != null) {
			try {
				result = mapper.readValue(cached, mapper.getTypeFactory().constructCollectionType(List.class, type));
			} catch (IOException e) {
				e.printStackTrace();
				result = retrieveFunc.get();
			}
			return result;
		}

		if (result == null) {
			result = retrieveFunc.get();
			try {
				String json = mapper.writeValueAsString(result);
				runWithJedis(jedis -> jedis.set(key, json));
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

		}

		return result;

	}
	
	@Override
	public <T> T fromCache(String key, Class<T> type, Supplier<T> retrieveFunc) {
		String cached = runWithJedis(jedis -> jedis.get(key));
		ObjectMapper mapper = new ObjectMapper();
		T value = null;
		
		if (cached != null) {
			try {
				value = mapper.readValue(cached, type);
			} catch (IOException e) {
				e.printStackTrace();
				value = retrieveFunc.get();
			}
			return value;
		}

		if (value == null) {
			value = retrieveFunc.get();
			try {
				String json = mapper.writeValueAsString(value);
				runWithJedis(jedis -> jedis.set(key, json));
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

		}

		return value;
	}

	@Override
	public Observable<String> observe(String channel) {
		
		return Observable.create(subscriber -> {
			final JedisPubSub pubSub = new JedisPubSub() {
	            @Override
	            public void onMessage(String channel, String message) {
	            	subscriber.onNext(message);
	            }
	        };

	        subscriber.add(Subscriptions.create(() -> {	   
	        	pubSub.unsubscribe();
	        	subscriber.onCompleted();
	        }));
	        
	        m_subscribePool.execute(new Runnable() {
	            @Override
	            public void run() {
	            	try(Jedis jedis = getJedis()) {
	            		jedis.subscribe(pubSub, channel);
	            	}
	            }
	        });
		});
	}
	
	@ConfigurationDependency(pid="org.amdatu.jedis")
	public void updated(Dictionary<String, Object> properties) throws ConfigurationException {
		if(properties != null) {
			String host = (String)properties.get("host");
			if(host == null) {
				throw new ConfigurationException("host", "Required property host is missing");
			}
			
			m_host = host;
			
			String port = (String)properties.get("port");
			if(port == null) {
				throw new ConfigurationException("port", "Required property port is missing");
			}
			
			m_port = Integer.parseInt(port);
		}
	}
}
