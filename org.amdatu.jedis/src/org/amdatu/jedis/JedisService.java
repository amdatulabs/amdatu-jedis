/*
 * Copyright (c) 2016 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jedis;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import redis.clients.jedis.Jedis;
import rx.Observable;

public interface JedisService {
	Jedis getJedis();
	<R> R runWithJedis(Function<Jedis, R> func);
	<T> List<T> listFromCache(String key, Class<T> type, Supplier<List<T>> retrieveFunc );
	<T> T fromCache(String key, Class<T> type, Supplier<T> retrieveFunc );
	Observable<String> observe(String channel);
}
